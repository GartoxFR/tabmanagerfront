import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TabManagerFront';

  tab = false;
  channelID = '';

  onKey(value: string) {
    this.channelID = value;
  }
}
